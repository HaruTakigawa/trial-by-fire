﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class NextCharacter : MonoBehaviour
{
    public GameObject Normal;
    public GameObject Slow;
    public GameObject Weak;
    public GameObject ExtUI2;
    public GameObject ExtUI3;
    private float timeNo;
    static public int seconds;
    static public int minutes;
    // Start is called before the first frame update
    void Start()
    {

        ExtUI2.SetActive(false);
        ExtUI3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        timeNo += Time.deltaTime;

        seconds = (int)(timeNo % 60);
        minutes = (int)(timeNo / 60);

        string timerString = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Normal")
        {
            Debug.Log("Normal is dead Slow is Alive");
            Normal.gameObject.SetActive(false);
            //Destroy(Normal);
            Slow.SetActive(true);
            ExtUI2.SetActive(true);
        }
        else if (col.gameObject.name == "Slow")
        {
            Debug.Log("Slow is dead Weak is Alive");
            Slow.SetActive(false);
            //Destroy(Slow);
            Weak.SetActive(true);
            ExtUI3.SetActive(true);
        }
        else if (col.gameObject.name == "Weak")
        {
            // WIN Everyone got out!
            //Destroy(Weak);
            Debug.Log("Weak is dead Everyone is Alive");
            StartCoroutine(NextScene());
        }
    }

    IEnumerator NextScene()
    {

        yield return SceneManager.LoadSceneAsync("WinScene", LoadSceneMode.Additive);
        yield return null;

        // Set the timer value here
        GameObject timerText = GameObject.Find("TimerText");
        timerText.GetComponent<Text>().text = string.Format("Escaped in: {0:00}:{1:00}", minutes, seconds);


        SceneManager.UnloadSceneAsync(gameObject.scene);
    }
}
