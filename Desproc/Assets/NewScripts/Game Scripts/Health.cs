﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public float MaxHealth;
    public float CurHealth;
    public float PercentageHealth;
    public float HealthRegen;
    public bool Respawnable;
    public bool Destroyed;
    public GameObject ParticleEffect;
    public GameObject Lighting;
    public GameObject Particles;
    private GameObject CurrentParticle;

    // Update is called once per frame
    void Update()
    {
        setPercentageHealth();
        regenHealth();
    }
    void setPercentageHealth()
    {
        PercentageHealth = CurHealth / MaxHealth;
    }
    void regenHealth()
    {
        if (CurHealth >= MaxHealth)
        {
            CurHealth = MaxHealth;
        }
        else
        {
            if (Destroyed) return;
            CurHealth = Mathf.Clamp(CurHealth + (HealthRegen * Time.deltaTime), 0, MaxHealth);
        }

    }
    public void TakeDamage(float damage)
    {
        if (CurHealth <= 0)
        {
            Debug.Log("Destroyed");
            if (Respawnable)
            {
                Destroyed = true;
                Lighting.gameObject.SetActive(false);
                Particles.gameObject.SetActive(false);
                this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }
            else
                Destroy(this.gameObject);
        }
        else
        {
            CurHealth -= damage;
            if (this.gameObject.layer == 9)
            {
                //FindObjectOfType<AudioManager>().Play("FireExtinguisher");
            }

            if (this.gameObject.layer == 8)
            {
                //FindObjectOfType<AudioManager>().Play("Axe");
                return;
            }
            Instantiate(ParticleEffect, this.transform.position, transform.rotation);
        }
    }
}
