﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ItemID
{
    public int UniqueItemID;
    public GameObject Item;
}

public class DGameManager : MonoBehaviour
{
    public static DGameManager Instance;

    public List<ItemID> ItemList;

    private int UniqueItemID;
    private int LevelItemID;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        if (Instance != this)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
        ResetItemID();
    }

    private void OnLevelWasLoaded(int level)
    {
        GameObject temp = null;
        foreach(FiremanWeapons weapon in FindObjectsOfType<FiremanWeapons>())
        {
            if (weapon.ItemLevelID == LevelItemID)
                temp = weapon.gameObject;
        }
        Destroy(temp);
        if(UniqueItemID != -1)
            LoadItem();
    }

    public void ResetItemID()
    {
        UniqueItemID = -1;
        LevelItemID = -1;
    }
    public void SetItemID(int UID, int LID)
    {
        UniqueItemID = UID;
        LevelItemID = LID;
    }
    public void LoadItem()
    {
        GameObject obj = Instantiate( ItemList.Find((x) => UniqueItemID == x.UniqueItemID).Item);
        FindObjectOfType<PlayerController2>().PickUpWeapon(obj.GetComponent<FiremanWeapons>());
    }
}
