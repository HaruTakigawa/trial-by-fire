﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public GameObject FireGameobject;
    public float SpawnArea;
    public float FireSpreadThreshold;
    public float SpeedIncreaseThreshold;
    public bool Spawned;
    public float TimeToRespawn;

    private float RespawnTimer;
    private Health health;
    void Start()
    {
        RespawnTimer = TimeToRespawn;
        health = this.gameObject.GetComponent<Health>();
    }
    void Update()
    {
        RespawnFire();
        if (this.GetComponent<Health>().Destroyed)
        {
            TimeToRespawn = TimeToRespawn - Time.deltaTime;
        }
        if (FireSpreadThreshold >= 100 && !Spawned)
        {
            Spawned = true;
            FireSpreadThreshold = 0;
            SpawnFire();
        }
        if (FireSpreadThreshold >= 100)
        {
            FireSpreadThreshold = 100;
        }
        else
        {
            if (health.Destroyed) return;
            FireSpreadThreshold = FireSpreadThreshold + (SpeedIncreaseThreshold * Time.deltaTime);
        }
    }
    void SpawnFire()
    {
        Vector3 RandomPosition = new Vector3(Random.Range(-SpawnArea / 2, SpawnArea / 2), Random.Range(-SpawnArea / 1000, SpawnArea / 1000), 0);
        Vector3 SpawnPoint = FireGameobject.transform.position + RandomPosition;
        GameObject SpawnedFire = (GameObject)Instantiate(FireGameobject, SpawnPoint, Quaternion.identity);
        Fire fire = SpawnedFire.GetComponent<Fire>();
        Health health = SpawnedFire.GetComponent<Health>();
        health.CurHealth = 20;
        health.Respawnable = false;
        fire.FireSpreadThreshold = 1;
        fire.Spawned = false;
        fire.transform.position = SpawnPoint;
    }
    void RespawnFire()
    {
        if (TimeToRespawn <= 0)
        {
            this.gameObject.SetActive(true);
            health.Destroyed = false;
            health.Lighting.gameObject.SetActive(true);
            health.Particles.gameObject.SetActive(true);
            this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
            this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
            TimeToRespawn = RespawnTimer;
        }
    }
}