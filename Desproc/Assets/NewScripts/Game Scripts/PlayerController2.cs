﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController2 : MonoBehaviour
{
    public bool m_FacingRight = true;
    public bool isMoving;
    [SerializeField] private float _speed;
    public Transform ItemSpawn;
    public FiremanWeapons currentItem;
    public float DamageMultiplier;
    public GameObject PickUpTransform;

    [HideInInspector] public bool IsUsingWeapon = false;

    public Collider2D[] collided;
    public Collider2D[] WeaponCollided;
    float horizontalMove = 0f;
    float verticalMove = 0f;
    Vector2 moveVelocity;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        PlayerMovement();
        if (Input.GetKeyDown(KeyCode.F) && currentItem == null)
        {
            Debug.Log("Item Picked Up");
            itemPickup();
        }
        else if (Input.GetKeyDown(KeyCode.F) && currentItem != null)
        {
            Debug.Log("Item Dropped");
            itemDrop();
        }
        Interact();
        UseItem();
    }
    void FixedUpdate()
    {
        rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
    }
    private void Interact()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            collided = new Collider2D[1];

            this.GetComponent<Collider2D>().OverlapCollider(new ContactFilter2D()
            {
                layerMask = LayerMask.GetMask("Interactable"),
                useTriggers = true,
                useLayerMask = true
            }, collided);

            if (collided[0] == null)
                return;

            else if (collided[0].GetComponent<Teleporter>())
            {
                collided[0].GetComponent<Teleporter>().Travel(this.gameObject);
            }
        }
    }

    public void itemDrop()
    {
        DGameManager.Instance.ResetItemID();
        currentItem.DropItem();
        currentItem = null;
    }
    public void itemPickup()
    {
        WeaponCollided = new Collider2D[1];
        this.GetComponent<Collider2D>().OverlapCollider(new ContactFilter2D()
        {
            layerMask = LayerMask.GetMask("Weapon"),
            useTriggers = true,
            useLayerMask = true
        }, WeaponCollided);

        if (WeaponCollided[0] == null) return;

        if (WeaponCollided[0].GetComponent<FiremanWeapons>())
        {
            Debug.Log("Weapon PickUp");
            PickUpWeapon(WeaponCollided[0].GetComponent<FiremanWeapons>());
            WeaponCollided[0].GetComponent<FiremanWeapons>().AttackPos = ItemSpawn;
        }
    }
    public void PickUpWeapon(FiremanWeapons weapon)
    {
        if (currentItem != null)
            return;

        DGameManager.Instance.SetItemID(weapon.UniqueID, weapon.ItemLevelID);
        weapon.PickUp(PickUpTransform);
        currentItem = weapon;
    }
    public void UseItem()
    {
        if (currentItem == null)
            return;

        if (Input.GetKey(KeyCode.Space))
        {
            IsUsingWeapon = true;
            currentItem.UseItem();
            if (currentItem.UniqueID == 1)
            {
                currentItem.CurUses = 0;
            }
            //Drops Extinguisher once it runs out of foam
            if (currentItem.UniqueID == 0 && currentItem.CurUses >= currentItem.MaxUses)
            {
                Debug.Log("Dropped Ext");
                itemDrop();
                IsUsingWeapon = false;
            }
        }
        else
        {
            IsUsingWeapon = false;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (currentItem.UniqueID == 1)
            {
                FindObjectOfType<AudioManager>().Play("Axe");
                FindObjectOfType<AudioManager>().StartLooping("Axe");
            }
            else if (currentItem.UniqueID == 0)
            {
                FindObjectOfType<AudioManager>().Play("FireExtinguisher");
                FindObjectOfType<AudioManager>().StartLooping("FireExtinguisher");
            }

        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (currentItem.UniqueID == 1)
            {
                FindObjectOfType<AudioManager>().Stop("Axe");
                FindObjectOfType<AudioManager>().StopLooping("Axe");
            }
            else if (currentItem.UniqueID == 0)
            {
                FindObjectOfType<AudioManager>().Stop("FireExtinguisher");
                FindObjectOfType<AudioManager>().StopLooping("FireExtinguisher");
            }
        }
    }
    private void Flip()
    {
        m_FacingRight = !m_FacingRight;

        transform.Rotate(0f, 180f, 0f);
    }
    public void PlayerMovement()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * _speed;
        verticalMove = Input.GetAxisRaw("Vertical") * _speed;

        Vector2 moveInput = new Vector2(horizontalMove, Input.GetAxisRaw("Vertical"));
        moveVelocity = moveInput.normalized * _speed;
        // Temporary code for animation trigger

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            isMoving = true;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            isMoving = true;
        }

        //Stopping walk animation
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            isMoving = false;
        }

        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            isMoving = false;
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            isMoving = false;
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            isMoving = false;
        }

        if (horizontalMove > 0 && !m_FacingRight)
        {
            Flip();
        }
        else if (horizontalMove < 0 && m_FacingRight)
        {
            Flip();
        }
    }
}



