﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorInteract : MonoBehaviour
{
    public string LevelName;

    public void OpenDoor()
    {
        SceneManager.LoadScene(LevelName);
    }
}
