﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public Teleporter TeleportTo;
    public bool CantUse;
    public void Travel(GameObject player)
    {
        if (CantUse) return;
        player.transform.position = TeleportTo.transform.position;


        //player.GetComponent<PlayerController2>().Move(TeleportTo.transform.position);
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<Health>() != null)
        {
            CantUse = true;
            TeleportTo.CantUse = true;
            //Debug.Log("Turned off collision");
            //TeleportTo.gameObject.GetComponent<Collider2D>().enabled = false;
            //this.gameObject.GetComponent<Collider2D>().enabled = false;
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<Health>() != null)
        {
            CantUse = false;
            TeleportTo.CantUse = false;
            //TeleportTo.gameObject.GetComponent<Collider2D>().enabled = true;
            //this.gameObject.GetComponent<Collider2D>().enabled = true;
        }
    }
}
