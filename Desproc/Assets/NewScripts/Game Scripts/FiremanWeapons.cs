﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiremanWeapons : MonoBehaviour
{
    public int UniqueID;
    public int ItemLevelID;
    public Transform AttackPos;
    public LayerMask Layer;
    public float AttackRange;
    public float Damage;
    public float MaxUses;
    public float CurUses;
    public bool CanAttack = true;
    public Vector3 DroppedItem;
    public Quaternion DroppedRotation;
    public void UseItem()
    {
        if (CanAttack && CurUses < MaxUses)
        {
            CurUses += 1 * Time.deltaTime;
            // Vector3 pos = this.transform.position + new Vector3(distance * Mathf.Sign(this.transform.lossyScale.normalized.x),0,0);
            Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(AttackPos.position, AttackRange, Layer);
            for (int i = 0; i < enemiesToDamage.Length; i++)
            {
                if (enemiesToDamage[i].GetComponent<Fire>())
                {
                    enemiesToDamage[i].GetComponent<Health>().TakeDamage(Damage);
                }
                else
                {
                    Debug.Log(Damage * this.gameObject.GetComponentInParent<PlayerController2>().DamageMultiplier);
                    enemiesToDamage[i].GetComponent<Health>().TakeDamage(Damage * this.gameObject.GetComponentInParent<PlayerController2>().DamageMultiplier);
                }
            }
        }
        else
        {
            Debug.Log("Out Of Uses");
        }
    }

    public void PickUp(GameObject Player)
    {
        this.GetComponent<Collider2D>().enabled = false;
        this.GetComponent<Rigidbody2D>().simulated = false;
        this.GetComponent<SpriteRenderer>().enabled = false;
        this.gameObject.transform.parent = Player.gameObject.transform;
        this.gameObject.transform.position = Player.gameObject.transform.position;
    }

    public void DropItem()
    {
        this.GetComponent<Collider2D>().enabled = true;
        this.GetComponent<Rigidbody2D>().simulated = true;
        this.GetComponent<SpriteRenderer>().enabled = true;
        this.gameObject.transform.parent = null;
        this.gameObject.transform.position += DroppedItem;
        this.gameObject.transform.rotation = DroppedRotation;
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        //Gizmos.DrawWireSphere(this.transform.position + new Vector3(distance * Mathf.Sign(this.transform.lossyScale.normalized.x), 0, 0), attackRange);
        Gizmos.DrawWireSphere(AttackPos.position, AttackRange);
    }
}
