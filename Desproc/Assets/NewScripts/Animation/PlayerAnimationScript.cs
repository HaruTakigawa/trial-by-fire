﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationScript : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Animator>().SetBool("IsWalking", this.GetComponent<PlayerController2>().isMoving);

        if (this.GetComponent<PlayerController2>().currentItem != null)
        {
            if (this.GetComponent<PlayerController2>().currentItem.UniqueID == 0)
            {
                this.GetComponent<Animator>().SetBool("IsExtinguishing", this.GetComponent<PlayerController2>().currentItem.UniqueID == 0 && this.GetComponent<PlayerController2>().IsUsingWeapon);
                this.GetComponent<Animator>().SetBool("IsPickingUp", this.GetComponent<PlayerController2>().currentItem.UniqueID == 0);

                if (this.GetComponent<PlayerController2>().currentItem.UniqueID == 0 && this.GetComponent<PlayerController2>().currentItem.CurUses >= this.GetComponent<PlayerController2>().currentItem.MaxUses)
                {
                    this.GetComponent<Animator>().SetBool("IsPickingUp", true);
                    this.GetComponent<Animator>().SetBool("IsExtinguishing", false);
                }
            }
            if (this.GetComponent<PlayerController2>().currentItem.UniqueID == 1)
            {
                this.GetComponent<Animator>().SetBool("IsChopping", this.GetComponent<PlayerController2>().currentItem.UniqueID == 1 && this.GetComponent<PlayerController2>().IsUsingWeapon);
                this.GetComponent<Animator>().SetBool("IsPickingUpAxe", this.GetComponent<PlayerController2>().currentItem.UniqueID == 1);
            }

            
        }
        /*
        if(this.GetComponent<PlayerController2>().currentItem != null)
        {
            this.GetComponent<Animator>().SetBool("IsChopping", this.GetComponent<PlayerController2>().currentItem.UniqueID == 1 && this.GetComponent<PlayerController2>().IsUsingWeapon);
            this.GetComponent<Animator>().SetBool("IsPickingUp", this.GetComponent<PlayerController2>().currentItem.UniqueID == 1);
        }*/
        else
        {
            this.GetComponent<Animator>().SetBool("IsPickingUp", false);
            this.GetComponent<Animator>().SetBool("IsExtinguishing", false);
            this.GetComponent<Animator>().SetBool("IsPickingUpAxe", false);
        }
    }
}
