﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisAnimation : MonoBehaviour
{
    public Health health;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        SwitchFormAnim();
    }

    void SwitchFormAnim()
    {
        if (health.CurHealth >= health.MaxHealth)
        {
            anim.SetBool("IsAtForm1", true);
            //Debug.Log("1st form");
        }

        if (health.CurHealth <= health.MaxHealth * 0.75 && health.CurHealth >= health.MaxHealth * 0.50)
        {
            anim.SetBool("IsAtForm2", true);
           //Debug.Log("2nd form");
        }

        if (health.CurHealth <= health.MaxHealth * 0.50)
        {
            anim.SetBool("IsAtForm3", true);
            //Debug.Log("3rd form");
        }
    }
}
