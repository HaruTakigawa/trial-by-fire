﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconPop : MonoBehaviour
{
    public GameObject Icon;
    // Start is called before the first frame update
    void Start()
    {
        Icon.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") &&  this.gameObject.CompareTag("Door"))
        {
            Icon.SetActive(true);
            Debug.Log("Icon up.");
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && this.gameObject.CompareTag("Door"))
        {
            Icon.SetActive(false);
        }
    }
}
