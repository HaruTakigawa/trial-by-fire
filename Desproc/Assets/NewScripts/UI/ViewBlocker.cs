﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewBlocker : MonoBehaviour
{
    public float CurTimer;
    public float MaxTimer = 600;
    public float t;

    public Transform Blocker;
    public Transform StartPoint;
    public Transform TargetLocation;
    public Transform MidPoint;

    public PlayerCoughs Cough;

    void Start()
    {
    }

    void Awake()
    {
        StartPoint.position = Blocker.position;
    }

    // Update is called once per frame
    void Update()
    {
        CurTimer += Time.deltaTime;
        t = CurTimer / MaxTimer;

        transform.position = Vector2.Lerp(StartPoint.position, TargetLocation.position, t);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "MidPoint")
        {
            Cough.MinTime -= 1;
            Cough.MaxTime -= 4;
        }
    }
}
