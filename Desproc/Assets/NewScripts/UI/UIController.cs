﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    //public TextMeshProUGUI TimeLeftText;
    //public TextMeshProUGUI TimeLeftTextShadow;
    public GameObject EquippedItemExtUI;
    public GameObject EquippedItemAxeUI;
    public GameObject ExtinguisherUI;
    public GameObject ControlsUI;
    public GameObject PauseUI;
    public GameObject PauseBtn;

    public TimerMeter TimeRemaining;


    //[SerializeField]
    //private Interaction FExtinguisher;

    //[SerializeField]
    //private Interaction FAxe;

    private PlayerController2 controller;
    // Start is called before the first frame update
    void Start()
    {
        //Time.timeScale = 0.0f;
        EquippedItemExtUI.SetActive(false);
        EquippedItemAxeUI.SetActive(false);
        ExtinguisherUI.SetActive(false);
        ControlsUI.SetActive(true);
        PauseUI.SetActive(false);
        controller = FindObjectOfType<PlayerController2>();
    }

    // Update is called once per frame
    void Update()
    {
        //TimeRemaining -= 1 * Time.deltaTime;
    }

    void LateUpdate()
    {
        controller = FindObjectOfType<PlayerController2>();
        if(controller.currentItem == null)
        {
            EquippedItemAxeUI.SetActive(false);
            EquippedItemExtUI.SetActive(false);
            ExtinguisherUI.SetActive(false);
        }
        else if (controller.currentItem.UniqueID == 0)
        {
            EquippedItemExtUI.SetActive(true);
            ExtinguisherUI.SetActive(true);
        }
        else
        { 
            EquippedItemAxeUI.SetActive(true);
            ExtinguisherUI.SetActive(false);
        }

    //TimeLeftText.text = ((int)TimeRemaining.CurTimer).ToString() + "/" + ((int)TimeRemaining.MaxTimer).ToString();
    //TimeLeftTextShadow.text = ((int)TimeRemaining.CurTimer).ToString() + "/" + ((int)TimeRemaining.MaxTimer).ToString();
    }
}