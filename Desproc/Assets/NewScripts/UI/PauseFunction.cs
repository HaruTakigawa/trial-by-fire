﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseFunction : MonoBehaviour
{
    [SerializeField]
    private UIController UI;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PauseBtn()
    {
        Time.timeScale = 0.0f;
        UI.PauseUI.SetActive(true);
        UI.PauseBtn.SetActive(false);
    }

    public void ResumeBtn()
    {
        UI.PauseUI.SetActive(false);
        UI.PauseBtn.SetActive(true);
        Time.timeScale = 1.0f;
    }

    public void QuitBtn()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void RetryBtn()
    {
        SceneManager.LoadScene("BuildingInterior");
    }

    public void MoveToIntroduction()
    {
        SceneManager.LoadScene("Introduction");
    }

    public void RetryTutorialBtn()
    {
        SceneManager.LoadScene("FireDept");
    }
}
