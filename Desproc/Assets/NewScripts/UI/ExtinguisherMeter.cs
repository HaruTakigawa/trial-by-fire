﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtinguisherMeter : MonoBehaviour
{
    private float MaxMeter;
    private float CurMeter;
    public Image BarImage;
    public PlayerController2 Player;

    void Update()
    {
        if (Player.currentItem == null)return;
        if (Player.currentItem.name == "FireExtinguisher")
        {
            CurMeter = Player.currentItem.GetComponent<FiremanWeapons>().CurUses;
            MaxMeter = Player.currentItem.GetComponent<FiremanWeapons>().MaxUses;
            //Debug.Log("Bar");
        }

        else
        {
            Debug.Log("No Item");
        }

        BarImage.fillAmount = CurMeter/MaxMeter;
    }
}
