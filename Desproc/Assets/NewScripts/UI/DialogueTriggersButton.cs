﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueTriggersButton : MonoBehaviour
{
    public GameObject ProceedToIntroductionButton;

    // Start is called before the first frame update
    void Start()
    {
        ProceedToIntroductionButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Fireman")
        {
            ProceedToIntroductionButton.SetActive(true);
        }
    }
}