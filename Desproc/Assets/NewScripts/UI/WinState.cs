﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class WinState : MonoBehaviour
{
    public GameObject Normal;
    public GameObject Slow;
    public GameObject Weak;
    public bool HasWon;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Normal.activeSelf && Slow.activeSelf && Weak.activeSelf)
        {
            HasWon = true;
            //SceneManager.LoadScene("WinScene");
        }
    }
}
