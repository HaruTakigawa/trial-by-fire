﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
       //Destroy(DGameManager.Instance.gameObject);
       //Destroy(TempSaveFire.instance.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartBtn()
    {
        SceneManager.LoadScene("FireDept");
    }

    public void ExitBtn()
    {
        Application.Quit();
    }
}
