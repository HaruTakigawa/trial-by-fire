﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class TimerText : MonoBehaviour
{
    public WinState Wingame;
    public TextMeshProUGUI TimeText;
    public TextMeshProUGUI TimeTextShadow;
    //public Text TestText;
    private float timeNo;
    static public int minutes;
    static public int seconds;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //CheckIfWin();
        timeNo += Time.deltaTime;

        seconds = (int)(timeNo % 60);
        minutes = (int)(timeNo / 60);

        string timerString = string.Format("{0:00}:{1:00}", minutes, seconds);

        TimeText.text = timerString;
        TimeTextShadow.text = timerString;

    }

    //IEnumerator NextScene()
    //{
    //    yield return SceneManager.LoadSceneAsync("WinScene", LoadSceneMode.Additive);
    //    yield return null;

    //    // Set the timer value here
    //    GameObject timerText = GameObject.Find("TimerText");
    //    timerText.GetComponent<Text>().text = string.Format("Escaped in: {0:00}:{1:00}", minutes, seconds);


    //    SceneManager.UnloadSceneAsync(gameObject.scene);
    //}

    //private void CheckIfWin()
    //{
    //    //// Temporary for testing
    //    //if (Input.GetKeyDown(KeyCode.P))
    //    //{
    //    //    StartCoroutine(NextScene());
    //    //}

    //    if (Wingame.HasWon)
    //    {
    //        Debug.Log("They won");
    //        StartCoroutine(NextScene());
    //    }

    //    if (!Wingame.HasWon)
    //    {
    //        Debug.Log("Still escaping");
    //        return;
    //    }
    //}
}
