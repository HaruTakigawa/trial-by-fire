﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueTrigger : MonoBehaviour
{
    // Goes to the different places that initiates a thought on the character
    public TextMeshProUGUI Sentence;
    public string FiremanDialogue;
    public string NormalDialogue;
    public string SlowDialogue;
    public string WeakDialogue;
    public Image FiremanPortrait;
    public Image NormalPortrait;
    public Image SlowPortrait;
    public Image WeakPortrait;
    private Image portrait;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Fireman")
        {
            portrait = FiremanPortrait;
            Sentence.text = FiremanDialogue;
        }
        else if (col.gameObject.name == "Normal")
        {
            portrait = NormalPortrait;
            Sentence.text = NormalDialogue;
        }
        else if (col.gameObject.name == "Slow")
        {
            portrait = SlowPortrait;
            Sentence.text = SlowDialogue;
        }
        else if (col.gameObject.name == "Weak")
        {
            portrait = WeakPortrait;
            Sentence.text = WeakDialogue;
        }

        if (portrait == null || Sentence.text == null) return;
        portrait.enabled = true;
        Sentence.enabled = true;
    }
    void OnTriggerExit2D(Collider2D col)
    {
        portrait.enabled = false;
        Sentence.enabled = false;
    }
}
